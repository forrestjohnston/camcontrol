# -*- coding: utf-8 -*- 
import os, re
from django.http import HttpResponse, HttpResponseServerError
from django.utils import simplejson
from django.core.servers.basehttp import FileWrapper
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render
from django.conf import settings
from django.template import RequestContext, loader
import cameractl
import logging
from urlparse import parse_qs
logger = logging.getLogger('camcontrol')

JSON_MIMETYPE="application/json"

def index(request):
	context = {'version': str(settings.VERSION) }
	return render(request, 'camcontrol/index.html', context)


def version(request):
	data = {'name': 'camcontrol', 'version' : '0.1'}
	return HttpResponse(simplejson.dumps(data), JSON_MIMETYPE)

		
@csrf_exempt  
def photo_snap (request):
	data = dict()
	if request.method == 'POST':
		data = simplejson.loads(request.body) if len(request.body) > 0 else dict()
		
		image_path = cameractl.snap_photo (data)

		if (image_path == None):
			return HttpResponseServerError()

		fwrap = FileWrapper(file(image_path, u'rb'))
		response = HttpResponse(fwrap,content_type='image/jpeg')
		response['Content-Disposition'] = 'attachment; filename=image.jpg'
		return response

	else:
		data = { 'msg' : 'Just allowed POST petition' }
		return HttpResponse(simplejson.dumps(data), JSON_MIMETYPE)

# TODO: This belongs in a utility module
def options_to_dict(line):
	"""
	Convert a single line of text with command options to dict. Example:
	Where line is
	'-t 500 -x -c 3 -d --long-option 456 -testing str-with-dashes'
	returns
		{
			'-d': True, '-c': '3', '-t': '500', 
			'--long-option': '456', '-x': True, '-testing': 'str-with-dashes'
		}
	"""
	matches = re.findall(r'(--?[\w-]+)(.*?)(?= -|$)', line)

	result = {}
	for match in matches:
		result[match[0]] = True if not match[1] else match[1].strip()
	
	return result


@csrf_exempt  
def photo_snapview (request):
	data = dict()
	if request.method == 'POST':
		logger.debug('photo_snapview request.body="%s"'%request.body)
		if len(request.body) > 0:
			bdict = parse_qs(request.body)
			# flatten dict values and place '--' on front of option keys
			# { 'key0': ['val0'], }  => { '--key0': 'val0', }
			opt_dict = dict()
			for (key, value) in bdict.iteritems():
				if not key.startswith('-'):
					key = '--%s' % key
					opt_dict[key] = value[0]
			if opt_dict.has_key('--cam_options'):
				# looks like: '-rot 270 --timeout 125 --thumb 64:48:37'
				parms = opt_dict['--cam_options']
				opt_dict.pop('--cam_options')
				data = options_to_dict(parms)
			else:
				data = opt_dict
				
			if opt_dict.has_key('--dimensions'):
				# convert form data to CameraOptions
				width, height = opt_dict['--dimensions'].split('x')
				width, height = int(width), int(height)	# error check
				opt_dict['--width'] = str(width)
				opt_dict['--height'] = str(height)
				opt_dict.pop('--dimensions')
				pass
		image_path = cameractl.snap_photo(data)

		if (image_path == None):
			return HttpResponseServerError()

		context = {'imagepath': str(image_path) }
		return render(request, 'camcontrol/snapview.html', context)

	else:
		data = { 'msg' : 'snapview allowed POST petition' }
		return HttpResponse(simplejson.dumps(data), JSON_MIMETYPE)

# next line seems to be necessary in order to have breakpoints work (?!)
@csrf_exempt
def video_streaming (request):

	if request.method == 'POST':
		json_data = simplejson.loads(request.raw_post_data)

		url_streaming = cameractl.start_streaming(json_data)

		return HttpResponse(simplejson.dumps(url_streaming), JSON_MIMETYPE)


	else:
		data = { 'msg' : 'Just allowed POST petition' }
		return HttpResponse(simplejson.dumps(data), JSON_MIMETYPE)


def video_streaming_stop (request):

	cameractl.stop_streaming()

	data = {'code': 200,
		'msg' : 'Streaming stopped correctly!' }
	return HttpResponse(simplejson.dumps(data), JSON_MIMETYPE)

