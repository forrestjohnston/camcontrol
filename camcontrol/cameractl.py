#!/usr/bin/env python

#  Copyright (C) 2013
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see http://www.gnu.org/licenses/.
#
#  Authors : Roberto Calvo <rocapal at gmail dot com>


import subprocess
from camera.raspicamera import CameraOptions
import socket
import threading
from django.conf import settings
import os.path, time	# debug 

import logging
logger = logging.getLogger('camcontrol')

if not settings.RASPI_CAM_AVAILABLE:
	system_call = lambda s, *args, **kw: 0
	logger.debug("raspiCam unavailable")
else:
	system_call = subprocess.call;
	logger.debug("raspiCam active")
	
IMAGE_FILE_PATH = settings.IMAGE_FILE_PATH # "/tmp/image.jpg"

RBPI_PHOTO_COMMAND = "/usr/bin/raspistill"
RBPI_VIDEO_COMMAND = "/usr/bin/raspivid"
RBPI_PHOTO_TIMEOUT = 125			# 1/8 sec warm-up

VLC_STREAMING_COMMAND = " | cvlc -vvv stream:///dev/stdin --sout '#standard{access=http,mux=ts,dst=:8090}' :demux=h264"
VLC_STREAMING_URL = "http://%s:8090/"

control_streaming_th = None 


def parse_args (args_dict=dict(), timeout=RBPI_PHOTO_TIMEOUT):
	args = ""
	keys = args_dict.keys()
	#  following is not parsing - belongs elsewhere
	#if '-t' not in keys and '--timeout' not in keys:
	#	args += "--timeout %s " % str(timeout)
	for key, val in args_dict.iteritems():
		if val == "true":
			args += '%s ' % key
		else:
			args += '%s %s ' % (key, val)
	return args

def snap_photo (args_dict):
	# validate the options by create/write of CameraOptions
	optslist = CameraOptions('--output %s %s' %
							(IMAGE_FILE_PATH, 
							parse_args(args_dict))).cmdline_options_list()
	#optstr = options.cmdline_options_list()
	command = '%s %s' % (RBPI_PHOTO_COMMAND, ' '.join(optslist))
	logger.debug("call: %s"%command)
	try:
		return_code = system_call(command, shell=True)
	except Exception as ex:
		logger.debug("subprocess.call: %s"%str(ex))
	if (return_code == 0):
		logger.debug("%s modified: %s" % \
					(IMAGE_FILE_PATH, 
					time.ctime(os.path.getmtime(IMAGE_FILE_PATH))))
		return IMAGE_FILE_PATH
	else:
		return None


def launch_cmd (command):
	code = system_call (command, shell=True)

def start_streaming (args_list):

	global control_streaming_th 
	res = {}
	args = " -o - " + parse_args(args_list, 9999999)
	command = RBPI_VIDEO_COMMAND + " " + args + " " + VLC_STREAMING_COMMAND

	print command 

	try:
		if (control_streaming_th == None):
			control_streaming_th = 1
			streaming_th  = threading.Thread(target = launch_cmd, args=[command])
			streaming_th.setDaemon(True)
			streaming_th.start()

		res["code"] = 200
		res["streaming_url"] = VLC_STREAMING_URL % (get_ip())
		
	except:
		res["code"] = 500
		res["msg"] = "Error while streaming was initialized!"
		res["streaming_url"] = ""
		

	return res

def stop_streaming():

	global control_streaming_th 

	cmds = {'vlc',RBPI_VIDEO_COMMAND}

	for cmd in cmds:
		print "killing " + cmd
		kill_command = "ps aux | grep " + cmd + " | grep -v grep | tr -s ' ' | cut -d' ' -f2 | tr -s '\n' ' ' | sed -e s/^/kill\ -9\ /g | bash"
		system_call (kill_command, shell=True)

	control_streaming_th = None



def get_ip ():
	s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
	s.connect(("gmail.com",80))
	ip = (s.getsockname()[0])
	s.close()
	return ip
