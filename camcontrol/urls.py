from django.conf.urls import patterns, include, url
from django.conf import settings
from django.contrib import admin
from django.conf.urls.static import static
admin.autodiscover()


urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'camcontrol.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    #url(r'^admin/', include(admin.site.urls)),
    # ex: /camcontrol/
    url(r'^$', 'camcontrol.views.index'),
    (r'^media/(?P<path>.*)$', 'django.views.static.serve',
                 {'document_root': settings.MEDIA_ROOT}),
    (r'^version/$', 'camcontrol.views.version'),
    (r'^photo/snap/$', 'camcontrol.views.photo_snap'),
    (r'^photo/snapview/$', 'camcontrol.views.photo_snapview'),
    (r'^video/streaming/$', 'camcontrol.views.video_streaming'),
    (r'^video/streaming/stop/$', 'camcontrol.views.video_streaming_stop'),
) + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
