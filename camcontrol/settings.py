"""
Django settings for camcontrol project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

import os
import subprocess

VERSION            = 1.0
BASE_DIR           = os.path.dirname(os.path.dirname(__file__))
CAMCONTROL_DIR     = os.path.join(BASE_DIR, 'camcontrol')
CAMCONTROL_LOG_DIR = os.path.join(CAMCONTROL_DIR, 'logs') 
MEDIA_ROOT         = os.path.join(CAMCONTROL_DIR, 'media')
IMAGE_FILE_PATH    = os.path.join(MEDIA_ROOT, 'image.jpg')
MEDIA_URL          = '/media/'

for d in (CAMCONTROL_DIR, CAMCONTROL_LOG_DIR, MEDIA_ROOT):
    if not os.path.exists(d):
        os.mkdir(d)


RASPI_CAM_AVAILABLE = False
try:
	subprocess.call('/usr/bin/raspistill -? 1>/tmp/settings_test 2>&1', shell=True)
	out = open('/tmp/settings_test', 'r').read()
	if 'usage: raspistill [options]' in out:
		RASPI_CAM_AVAILABLE = True
except Exception as ex:
	RASPI_CAM_AVAILABLE = False
	
# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '%ksd=8(#r1sqn3fnfowm5*gc+4n9cf7sy=m2rr#&+9)nqhj&@p'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
)

TEMPLATE_DIRS = [os.path.join(CAMCONTROL_DIR, 'templates')]

# When debugging in Eclipse, we are using 'settings.py runserver' and
#   so we dont need south and gunicorn.  See this file header for more info.
if not os.name == 'nt':
    INSTALLED_APPS = INSTALLED_APPS + ('south', 'gunicorn',)


MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'camcontrol.urls'

WSGI_APPLICATION = 'camcontrol.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(CAMCONTROL_DIR, 'camcontrol_sqlite3.db'),
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/
LANGUAGE_CODE = 'en-us'
TIME_ZONE = 'UTC'
USE_I18N = True
USE_L10N = True
USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, "static")
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    os.path.join(os.path.dirname(__file__),'static').replace('\\','/'),
)

LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
        'forrest': {
            'format': '%(asctime)s %(levelname)s %(module)s-%(lineno)s: %(message)s',
            'datefmt' : "%Y%m%d-%H:%M:%S"
        },
    },
    'handlers': {
        'null': {
            'level': 'DEBUG',
            'class': 'logging.NullHandler',
        },
        'file': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': os.path.join(CAMCONTROL_LOG_DIR, 'debug.log'),
            'formatter': 'forrest'
        },
        'console':{
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'simple'
        },
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler',
            'filters': [],
        }
    },
    'loggers': {
        'camcontrol': {
            'handlers': ['file'],
            'propagate': True,
            'level': 'DEBUG',
        },
        'django': {
            'handlers': ['file'],
            'propagate': True,
            'level': 'DEBUG',
        },
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': False,
        },
    }
}


