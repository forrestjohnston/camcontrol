raspiCamControl
===============

REST api to control Raspberry Pi Camera implemented with Django framework.

Forked from https://github.com/rocapal/RBPICameraRest.git 2014/05/03
Renamed RBPICameraRest to raspiCamControl
